#!/bin/sh

MENU_OPTS=("Logout" "Kexec" "Shutdown" "Reboot")

if [ $# -eq 0 ]; then
  exit 1
fi

SELECTION=$(for i in "${MENU_OPTS[@]}"; do
  echo $i
done | rofi -p "Session" -dmenu | tr '[:upper:]' '[:lower:]')

verify () {
  VSEL=$(echo -e "No\nYes" | 
    rofi -p "Confirm $SELECTION?" -dmenu)
  if [ "$VSEL" == "Yes" ]; then
    $@
  fi
}

case $SELECTION in
  logout)
    verify "$@"
  ;;
  lock)
    swaylock -f
  ;;
  shutdown)
    verify "systemctl poweroff"
  ;;
  reboot)
    verify "systemctl reboot"
  ;;
  kexec)
    verify "systemctl kexec"
  ;;
esac

