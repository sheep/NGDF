#!/bin/bash

FIFO=~/.config/wob/wob.fifo

[ ! -p "$FIFO" ] && mkfifo $FIFO

case "$(cat /etc/hostname)" in
    "Cewri")
        cfg="-q --min-value=10%"
    ;;
    *)
        cfg="-q"
    ;;
esac

case $1 in
    up|u)
        brightnessctl $cfg set +10%
        ;;
    down|dn|d)
        brightnessctl $cfg set 10%-
        ;;
esac

BRI=$(brightnessctl -m | awk -F',' '{gsub("%",""); print $4 }')

echo "$BRI brightness" >> $FIFO
