#!/bin/bash

FIFO=~/.config/wob/wob.fifo

[ ! -p "$FIFO" ] && mkfifo $FIFO

NAME="volume"

case $1 in
    up|u)
        pamixer -i 10
        ;;
    down|dn|d)
        pamixer -d 10
        ;;
    mute|m)
        pamixer --toggle-mute
        ;;
    mic)
        pamixer --default-source --toggle-mute
        VOL=$(pamixer --default-source --get-volume)
        if [ "$(pamixer --default-source --get-mute)" == "true" ]; then
            NAME="mic_m"
        else
            NAME="mic"
        fi
        ;;
esac

[ -z $VOL ] && VOL=$(pamixer --get-volume)
[[ "$(pamixer --get-mute)" == "true" && $1 != "mic" ]] && NAME="mute"

echo "$VOL $NAME" >> $FIFO
