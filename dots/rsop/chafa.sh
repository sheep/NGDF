#!/bin/sh
# Chafa thumbnailer for RSOP
thumb() {
    CACHE="${XDG_CACHE_HOME:-$HOME/.cache}/lf/thumb.$(stat --printf '%n\0%i\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | b3sum | cut -d' ' -f1)"
    [ ! -f "$CACHE" ] && ffmpegthumbnailer -i "$1" -o "$CACHE" -s 1024 1> /dev/null 2> /dev/null
    echo $CACHE
}

IMGTHUMB=$(thumb "${1}")
IMGWIDTH=$(echo "${2} * 0.95" | bc)
IMGHEIGHT=$(echo "${3} * 0.95" | bc)

# Write sixel images to terminal
chafa --polite on --animate off --threshold 1 --format sixel \
      --work 9 --scale 1.0 --colors full \
      --size "${IMGWIDTH}x${IMGHEIGHT}" "${IMGTHUMB}"

# Provide metadata. Currently disabled.
printf "\n"
