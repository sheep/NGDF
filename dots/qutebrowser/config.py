# Generalized config for qutebrowser.

import json
import os

print("Config loaded!")

# Ignore autoconfig
config.load_autoconfig(False)

# Load agents json into config
with open(config.configdir / 'useragents.json', 'r') as jsonfile:
    agents = json.load(jsonfile)

# Load .py configs
for file in os.listdir(config.configdir):
    if file.endswith(".py") and file != "config.py":
        print("Loading " + file)
        config.source(file)

term = 'footclient'
editor = 'helix'
startpage = 'https://start.duckduckgo.com'

# Generalized settings
cc = c.content
c.editor.command = [term, '-e', editor, '{file}']
c.tabs.favicons.show = 'never'
c.tabs.indicator.width = 0
c.tabs.pinned.shrink = False
c.tabs.title.alignment = 'center'
c.colors.webpage.preferred_color_scheme = 'dark'
c.auto_save.session = cc.mute = True
cc.autoplay = cc.notifications.enabled = cc.register_protocol_handler = False
cc.webrtc_ip_handling_policy = 'default-public-interface-only'
c.hints.chars = 'asdfgqwertzxcvb'
c.statusbar.show = 'always'
c.tabs.last_close = 'close'
c.tabs.show = 'always'
c.tabs.mousewheel_switching = False
c.scrolling.smooth = True
c.tabs.title.format = '{index}: {current_title} {audio}'
c.tabs.title.format_pinned = '󰐃 {index}: {current_title} {audio}'
c.completion.open_categories = [
    'bookmarks', 'quickmarks', 'searchengines', 'history', 'filesystem']
c.tabs.padding = {'bottom': 5, 'left': 3, 'right': 3, 'top': 3}
c.statusbar.padding = {'bottom': 3, 'left': 3, 'right': 3, 'top': 5}
c.downloads.position = 'bottom'
c.statusbar.widgets = ['url', 'history', 'progress']
c.spellcheck.languages = ['en-US']
c.url.default_page = c.url.start_pages = startpage

# Adblock and privacy
cc.blocking.method = 'both'
cc.blocking.adblock.lists = [
    'https://easylist.to/easylist/easylist.txt',
    'https://easylist.to/easylist/easyprivacy.txt',
    'https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt',
    'https://pgl.yoyo.org/adservers/serverlist.php'
    + '?hostformat=adblockplus&mimetype=plaintext'
]
cc.cookies.accept = 'no-3rdparty'

# Fetch most recent user agent for firefox.
cc.headers.user_agent = agents['@modern']['chrome']['win10'][0]

hl = 'hint links'
hs = hl + ' spawn'
hu = '{hint-url}'
sc = 'cmd-set-text -s'


# Bind loop
def bloop(cmd: list):
    assembled_str = ''
    for x in cmd:
        assembled_str += x + ' '
    return assembled_str


# Bind map
binds = {
    ';c': 'clear-messages',
    ';k': 'hint --rapid',
    ';p': [hl, 'run :open --private', hu],
    ';m': [hs, 'mpv --pause', hu],
    ';M': [hs, 'mpv', hu],
    ';n': [hs, 'mpv --pause --profile=faster', hu],
    ';N': [hs, 'mpv --pause --profile=fast', hu],
    ';q': [hs, 'qrencode -t png -o /tmp/qr.png', hu],
    ';x': [
        hs, term,
        '-e bash -c "sleep 0.1; yt-dlp --get-description',
        hu, '| less"'],
    'b': [sc, ':bookmark-add {url:pretty}'],
    'm': [sc, ':quickmark-add {url:pretty}'],
    '<Ctrl+Tab>': 'tab-next',
    '<Ctrl+Shift+Tab>': 'tab-prev',
    '<Ctrl+o>': [sc, ':open -p'],
    '<F12>': 'config-clear ;; config-source',
    '<Alt-f>': 'fullscreen',
}

for item in binds:
    if type(binds[item]) is list:
        binds[item] = bloop(binds[item])
    config.bind(item, binds[item])

# Remove because I use other controls.
unbinds = ['<Ctrl+q>', '<F5>', '<F11>']
for item in unbinds:
    config.unbind(item)
