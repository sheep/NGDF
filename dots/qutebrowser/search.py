# Search engine setup

h = 'https://'
default = h + 'start.duckduckgo.com/?q={}'

# Chinese tea site search
zh_se = h + 'google.com/search?q={}+'
zh_sites = ['jadepot.com.cn', 'jadepot.com',
            'xuite.net', 'potsart.com',
            'p9.com.tw', 'teaart.com.tw']

# Loop
for i in range(0, len(zh_sites)):
    if len(zh_sites)-1 == i:
        zh_se += 'site:' + zh_sites[i]
    else:
        zh_se += 'site:' + zh_sites[i] + '+OR+'

cantodict = h + 'cantonese.sheik.co.uk/dictionary/search/?text={}&searchtype='

c.url.searchengines = {
    'DEFAULT': default,
    'aw': h + 'wiki.archlinux.org/?search={}',
    'suse': h + 'software.opensuse.org/search?baseproject=ALL&q={}',
    'wrt': h + 'openwrt.org/?q={}&do=search',
    'w': h + 'en.wikipedia.org/wiki/?search={}',
    'wk': h + 'en.wiktionary.org/w/?search={}',
    'cdj': cantodict + '3',
    'cde': cantodict + '4',
    'cdw': cantodict + '1',
    'yt': h + 'youtube.com/results?search_query={}',
    'inv': h + 'inv.vern.cc/search?q={}',
    'ebay': h + 'ebay.com/sch/i.html?_nkw={}',
    'amazon': h + 'amazon.com/s?k={}',
    'zhtea': zh_se
}
