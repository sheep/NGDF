# Place content at left side.

c.tabs.favicons.show = 'always'
c.tabs.title.alignment = 'right'
c.tabs.position = 'left'
c.tabs.width = 48
c.tabs.title.format = '{index}'
c.tabs.title.format_pinned = '󰐃 {index}'
