// ==UserScript==
// @name        Genius to Dumb
// @description Redirect Genius links to Dumb
// @namespace   io.github.photosheep.GeniusDumbify
// @match       *://genius.com/*
// @match       *://*.genius.com/*
// @run-at      document-start
// @grant       none
// @version     1.0
// @author      smolsheep
// ==/UserScript==
 
const instance = "https://dumb.lunar.icu";

let genius = new URL(window.location);
let dumb = new URL(instance);

// Just slap old pathname on new site
dumb.pathname = genius.pathname;

location.href = dumb.toString();
