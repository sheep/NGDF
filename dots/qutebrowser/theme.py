# Theme and overrides.

# Import python theme package
from catppuccin import PALETTE
theme = PALETTE.frappe.colors

# Font
cf = c.fonts
cf.default_family = 'Hack Nerd Font Mono'
cf.default_size = '10pt'
cf.tabs.selected = 'bold default_size default_family'
cf.downloads = 'bold default_size default_family'
cf.statusbar = 'bold default_size default_family'

# Stock
ccol = c.colors

# Tabs
tabs = ccol.tabs
tabs.bar.bg = theme.base.hex
tabs.even.bg = tabs.odd.bg = theme.base.hex
tabs.pinned.even.bg = tabs.pinned.odd.bg = theme.base.hex
tabs.even.fg = tabs.odd.fg = theme.text.hex
tabs.pinned.even.fg = tabs.pinned.odd.fg = theme.text.hex
tabs.selected.even.bg = tabs.selected.odd.bg = theme.base.hex
tabs.pinned.selected.even.bg = tabs.pinned.selected.odd.bg = theme.base.hex
tabs.selected.even.fg = tabs.selected.odd.fg = theme.pink.hex
tabs.pinned.selected.even.fg = tabs.pinned.selected.odd.fg = theme.pink.hex

ccol.tabs.indicator.error = theme.red.hex
ccol.tabs.indicator.system = "none"

# Completion
comp = ccol.completion
compcat = comp.category
comp.even.bg = theme.mantle.hex
comp.odd.bg = theme.crust.hex
comp.fg = theme.subtext0.hex
comp.item.selected.bg = theme.surface2.hex
comp.item.selected.border.bottom = theme.surface2.hex
comp.item.selected.border.top = theme.surface2.hex
comp.item.selected.fg = theme.text.hex
comp.item.selected.match.fg = theme.rosewater.hex
comp.match.fg = theme.text.hex
comp.scrollbar.bg = theme.crust.hex
comp.scrollbar.fg = theme.surface2.hex
compcat.bg = theme.base.hex
compcat.border.bottom = theme.mantle.hex
compcat.border.top = theme.overlay2.hex
compcat.fg = theme.green.hex

# Downloads
down = ccol.downloads
down.bar.bg = theme.base.hex
down.error.bg = theme.base.hex
down.start.bg = theme.base.hex
down.stop.bg = theme.base.hex
down.error.fg = theme.red.hex
down.start.fg = theme.blue.hex
down.stop.fg = theme.green.hex
down.system.fg = "none"
down.system.bg = "none"

# Statusbar
status = ccol.statusbar
status.normal.bg = status.insert.bg = status.command.bg = theme.base.hex
status.caret.bg = status.caret.selection.bg = status.progress.bg = theme.base.hex
status.passthrough.bg = status.command.private.bg = theme.base.hex
status.insert.fg = status.url.success.https.fg = theme.green.hex
status.normal.fg = status.command.fg = status.url.fg = theme.text.hex
status.passthrough.fg = status.caret.fg = theme.peach.hex
status.caret.selection.fg = theme.peach.hex
status.url.error.fg = theme.red.hex
status.url.hover.fg = theme.sky.hex
status.url.success.http.fg = theme.teal.hex
status.url.warn.fg = theme.yellow.hex
status.private.bg = theme.mantle.hex
status.private.fg = status.command.private.fg = theme.subtext1.hex

# Hints
c.hints.border = '1px solid ' + theme.overlay2.hex
ccol.hints.bg = theme.base.hex
ccol.hints.fg = theme.text.hex
ccol.hints.match.fg = theme.pink.hex
ccol.keyhint.bg = theme.mantle.hex
ccol.keyhint.fg = theme.text.hex
ccol.keyhint.suffix.fg = theme.subtext1.hex

# Messages
messages = ccol.messages
messages.error.bg = theme.overlay0.hex
messages.info.bg = theme.overlay0.hex
messages.warning.bg = theme.overlay0.hex
messages.error.border = theme.mantle.hex
messages.info.border = theme.mantle.hex
messages.warning.border = theme.mantle.hex
messages.error.fg = theme.red.hex
messages.info.fg = theme.text.hex
messages.warning.fg = theme.peach.hex

# Prompts
ccol.prompts.bg = theme.mantle.hex
ccol.prompts.border = "1px solid " + theme.overlay0.hex
ccol.prompts.fg = theme.text.hex
ccol.prompts.selected.bg = theme.surface2.hex
ccol.prompts.selected.fg = theme.rosewater.hex

# Contextmenu
context = ccol.contextmenu
context.menu.bg = theme.base.hex
context.menu.fg = theme.text.hex
context.disabled.bg = theme.mantle.hex
context.disabled.fg = theme.overlay0.hex
context.selected.bg = theme.overlay0.hex
context.selected.fg = theme.rosewater.hex

# Default
ccol.webpage.bg = 'white'
