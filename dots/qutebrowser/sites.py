# Per site configs.

# Always enable geolocation on startpage and homeassistant page.
config.set("content.geolocation", True, '*://localhost/*')
config.set("content.geolocation", True, '*://homeassistant.local/*')
