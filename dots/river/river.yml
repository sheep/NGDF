## Global config
init: [
  'bash $HOME/.config/sway/scripts/gsettings.sh',
  'dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK QT_QPA_PLATFORMTHEME QT_QPA_PLATFORM XDG_SESSION_DESKTOP XDG_CURRENT_DESKTOP',
  '/usr/libexec/polkit-gnome-authentication-agent-1',
  'pamixer --default-source --mute',
  'swayidle -w',
  'wayneko --layer top --follow-pointer false --outline-colour 0x232634 --background-colour 0xc6d0f5 --survive-close',
  'foot --server',
  'tail -f $HOME/.config/wob/wob.fifo | wob',

  'waybar',
  'dunst',
  'rivertile',
  ]

other: [
  'declare-mode passthrough',
  'map-pointer normal Mod4 BTN_LEFT move-view',
  'map-pointer normal Mod4 BTN_RIGHT resize-view',
  ]

props: 
  keyboard-layout: '-options caps:escape,compose:prsc,shift:both_capslock_cancel us'
  set-repeat: '25 300'
  hide-cursor: 'when-typing enabled'
  hide-cursor: 'timeout 6000'
  set-cursor-warp: 'on-output-change'
  default-attach-mode: 'bottom'
  output-layout: 'rivertile'
  default-layout: 'rivertile'
  background-color: '0x232634'
  border-color-focused: '0xf4b8e4'
  border-color-unfocused: '0x303446'
  border-color-urgent: '0x8caaee'
  border-width: 2

binds:
  ## River controls
  - modifier: Super
    key: F
    action: toggle-fullscreen

  - modifier: Super+Shift
    key: R
    type: spawn
    cmd: ~/.config/river/init.lua reload

  - modifier: Super
    key: Space
    action: toggle-float

  - modifier: Super
    key: Z
    action: zoom

  - modifier: Super+Shift
    key: Q
    action: close

  - modifier: Super
    key: J
    action: focus-view next

  - modifier: Super+Shift
    key: J
    action: swap next

  - modifier: Super
    key: K
    action: focus-view previous

  - modifier: Super+Shift
    key: K
    action: swap previous

  - modifier: Super
    key: N
    action: focus-output previous

  - modifier: Super+Shift
    key: N
    action: send-to-output previous

  - modifier: Super
    key: M
    action: focus-output next

  - modifier: Super+Shift
    key: M
    action: send-to-output next

  # Control rivertile
  - modifier: Super
    key: L
    action: send-layout-cmd rivertile "main-ratio +0.1"

  - modifier: Super+Shift
    key: L
    action: send-layout-cmd rivertile "main-count +1"

  - modifier: Super
    key: H
    action: send-layout-cmd rivertile "main-ratio -0.1"

  - modifier: Super+Shift
    key: H
    action: send-layout-cmd rivertile "main-count -1"

  - modifier: Super+Control
    key: H
    action: send-layout-cmd rivertile "main-location left"

  - modifier: Super+Control
    key: L
    action: send-layout-cmd rivertile "main-location right"

  - modifier: Super+Control
    key: K
    action: send-layout-cmd rivertile "main-location top"

  - modifier: Super+Control
    key: J
    action: send-layout-cmd rivertile "main-location bottom"

  # Execs
  - modifier: Super
    key: Backspace
    type: spawn
    cmd: loginctl lock-session

  - modifier: Super
    key: D
    type: spawn
    cmd: tofi-drun | xargs -I'{}' riverctl spawn '{}'

  - modifier: Super
    key: Return
    type: spawn
    cmd: footclient

  - modifier: Super
    key: O
    type: spawn
    cmd: nemo

  # Music controls
  - key: XF86AudioRaiseVolume
    type: spawn
    cmd: ~/.config/wob/volume.sh u
    modes: ['normal', 'locked']

  - key: XF86AudioLowerVolume
    type: spawn
    cmd: ~/.config/wob/volume.sh d
    modes: ['normal', 'locked']

  - key: XF6MonBrightnessUp
    type: spawn
    cmd: ~/.config/wob/brightness.sh up
    modes: ['normal', 'locked']

  - key: XF6MonBrightnessDown
    type: spawn
    cmd: ~/.config/wob/brightness.sh d
    modes: ['normal', 'locked']

  - key: XF86AudioMute
    type: spawn
    cmd: ~/.config/wob/volume.sh mute
    modes: ['normal', 'locked']

  - key: XF86AudioMicMute
    type: spawn
    cmd: ~/.config/wob/volume.sh mic
    modes: ['normal', 'locked']

  - modifier: Super
    key: Left
    type: spawn
    cmd: playerctl previous
    modes: ['normal', 'locked']

  - modifier: Super
    key: Right
    type: spawn
    cmd: playerctl next
    modes: ['normal', 'locked']

  - modifier: Super
    key: Up
    type: spawn
    cmd: playerctl play-pause
    modes: ['normal', 'locked']

  - modifier: Super
    key: Down
    type: spawn
    cmd: playerctl play-pause
    modes: ['normal', 'locked']

  # Notif controls
  - modifier: Super
    key: Bracketleft
    type: spawn
    cmd: dunstctl history-pop

  - modifier: Super
    key: Bracketright
    type: spawn
    cmd: dunstctl close

  - modifier: Super+Shift
    key: Bracketright
    type: spawn
    cmd: dunstctl close-all

  - modifier: Super
    key: Backslash
    type: spawn
    cmd: dunstctl set-paused toggle

  - modifier: Super
    key: C
    type: spawn
    cmd: ~/.config/sway/scripts/screenshot.sh all

  - modifier: Super+Shift
    key: C
    type: spawn
    cmd: ~/.config/sway/scripts/screenshot.sh screen

  ## Modes
  # Add passthrough mode
  - modifier: Super
    key: F11
    action: enter-mode passthrough

  - modifier: Super
    key: F11
    action: enter-mode normal
    modes: ['passthrough']

  # Exit
  - modifier: Super
    key: F12
    type: spawn
    cmd: ~/.config/tofi/tofi_session.sh riverctl exit
