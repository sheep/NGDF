#!/usr/bin/luajit

local args = { ... }
print('Init started!')

-- Include rust YAML library
YAML = require('lyaml')

-- Lua5.1/Luajit don't support native bitwise.
B32 = require('bit32')

-- Path of script
local path = debug.getinfo(1).source:match('@?(.*/)')

-- Read river yml config
local global_yml = assert(io.open(path .. 'river.yml', 'r'))
local global_contents = global_yml:read('*all')
global_yml:close()
local config = YAML.load(global_contents)

-- Include per-host configs
local hostname = os.getenv('HOSTNAME')
local local_yml = assert(io.open(path .. 'host/' .. hostname .. '.yml', 'r'))
local local_contents = local_yml:read('*all')
local_yml:close()
local host_config = YAML.load(local_contents)

-- Add more configs up above if needed.
local conflist = { config, host_config }

-- Mode list for shorter calls
local ml = { n = 'normal', l = 'locked' }

local riverctl = function(call, ...)
  local arg, string, command = { ... }, '', ''
  for _, y in ipairs(arg) do
    -- Appends space at end
    string = string .. y .. ' '
  end
  if call == 'prop' then
    command = string.format('riverctl %s', string)
  elseif call == 'map' then
    command = string.format('riverctl map %s', string)
  elseif call == 'spawn' then
    command = string.format('riverctl spawn "%s"', string)
  end
  command = string.format('%s 2>/dev/null', command)
  print('\t' .. command)
  os.execute(command)
end

-- riverctl mapping function
local function river_map(modes, bind, cmd)
  for mode in pairs(modes) do
    local cmdstr = modes[mode] .. ' ' .. bind .. ' ' .. cmd
    riverctl('map', cmdstr)
  end
end

print("Binding workspaces:")
-- Create workspace binds
for i = 1, 9 do
  local tag_num = B32.lshift(1, i - 1)

  -- Super+[1-9] to focus tag [0-8]
  river_map({ ml.n }, 'Super ' .. i, 'set-focused-tags ' .. tag_num)

  -- Super+Shift+[1-9] to tag focused view with tag [0-8]
  river_map({ ml.n }, 'Super+Shift ' .. i, 'set-view-tags ' .. tag_num)

  -- Super+Control+[1-9] to toggle focus of tag [0-8]
  river_map({ ml.n }, 'Super+Control ' .. i, 'toggle-focused-tags ' .. tag_num)

  -- Super+Alt+[1-9] to toggle tag [0-8] of focused view
  river_map({ ml.n }, 'Super+Alt ' .. i, 'toggle-view-tags ' .. tag_num)
end

-- Show all tags, b32 only supports 31 bits for math
-- all_tags is hardcoded to the result of (1 >> 32) - 1
local all_tags = 4294967295
river_map({ ml.n }, 'Super 0', 'set-focused-tags ' .. all_tags)
river_map({ ml.n }, 'Super+Shift 0', 'set-view-tags ' .. all_tags)

-- Loop between arrays
for n, array in ipairs(conflist) do
  print('Config ' .. n .. ':')
  if array['props'] then
    for item, prop in pairs(array['props']) do
      riverctl('prop', item, prop)
    end
  end

  if array['input'] then
    for device, values in pairs(array['input']) do
      for setting, value in pairs(values) do
        riverctl('prop', 'input', device, setting, value)
      end
    end
  end

  if array['binds'] then
    print('Setting river binds from binds:')
    for _, bind in pairs(array['binds']) do
      -- Checks if empty. Blank appears as table.
      if bind['modifier'] == nil or type(bind['modifier']) == 'table' then
        bind['modifier'] = 'None'
      end

      local keys = bind['modifier'] .. ' ' .. bind['key']

      -- If no mode defined in config, bind to normal.
      if bind['modes'] == nil then
        bind['modes'] = { ml.n }
      end

      -- Check if spawner, build commands.
      local cmd = ''
      if bind['type'] == 'spawn' then
        cmd = string.format('spawn "%s"', bind['cmd'])
      else
        cmd = bind['action']
      end

      river_map(bind['modes'], keys, cmd)
    end
  end

  if array['other'] then
    for _, item in pairs(array['other']) do
      riverctl('prop', item)
    end
  end

  if args[1] ~= 'reload' then
    if array['init'] then
      print('Running init:')
      for _, cmd in ipairs(array['init']) do
        riverctl('spawn', cmd)
      end
    end
  end

  if array == nil then
    print('\tNo commands present')
  end
end
print("\n---\nEnd\n---")
