#!/bin/bash

MENU_OPTS=("Logout" "Kexec" "Shutdown" "Reboot")

if [ $# -eq 0 ]; then
  exit 1
fi

SELECTION=$(printf "%s\n" "${MENU_OPTS[@]}" |
   tofi --prompt-text "Session" | tr '[:upper:]' '[:lower:]')

verify () {
  VSEL=$(echo -e "No\nYes" | 
    tofi --prompt-text "Confirm $SELECTION?")
  if [ "$VSEL" == "Yes" ]; then
    $@
  fi
}

case $SELECTION in
  logout)
    verify "$@"
  ;;
  lock)
    loginctl lock-session
  ;;
  shutdown)
    verify "systemctl poweroff"
  ;;
  reboot)
    verify "systemctl reboot"
  ;;
  kexec)
    verify "systemctl start kexec.target"
  ;;
esac

