#!/bin/ruby

width = 63

# Wrap each piped line with regex.
ARGF.each do |line|
  puts '  * ' + line.gsub(/(.{1,#{width}})( +|$)\n?|(.{#{width}})/, "\\1\\3\n    ").strip
end
