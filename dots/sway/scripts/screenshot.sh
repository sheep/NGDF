#!/bin/bash

file=~/Captures/$(date '+%F_%H%M%S').webp

echo $file
case "$1" in
    region) grim -g "$(slurp)" $file ;;
    screen) grim -g "$(slurp -o)" $file ;;
    *|all) grim $file ;;
esac

# This undoes notification if you cancelled at any point, so no file is saved.
if [ -f $file ]; then
    dunstify -i $file "Screenshot taken!" "File is $(basename $file)"
fi
