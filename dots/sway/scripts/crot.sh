#!/bin/sh
# Rotation script specifically for Cewri
# Please avoid running on your system without heavy modifications

# Builtin panel
PANEL='MS_ BW8 0x00000001'

# Builtin peripherals
KB='1:2:AT_Raw_Set_2_keyboard'
TP='2321:21128:SYNA3602:00_0911:5288_Touchpad'

# Controls enablement of laptop parts
parttoggle() {
  for i in $TP
  do
    swaymsg input $i events $1
  done
}

# Handle rotation of panel
transform () {
  # Tell swaymsg to transform the panel
  swaymsg "output \"${PANEL}\" transform $1"

  # If normal mode, enable keyboard and mouse
  if [ $1 -eq "0" ]; then
    ev="enabled"
  else 
    ev="disabled"
  fi

  # Handle
  parttoggle $ev
}

# Take inputs as variables and handle rotation
case $1 in
  right|r)
    transform 90
  ;;
  flip|f)
    transform 180
  ;;
  left|l)
    transform 270
  ;;
  nopart|np)
    transform 0
    parttoggle "disabled"
  ;;
  # Has to be at end to handle wildcard
  normal|n|*)
    transform 0
  ;;
esac
