#!/bin/bash

gnome_schema="org.gnome.desktop.interface"

gsettings set "$gnome_schema" gtk-theme "catppuccin-frappe-pink-standard+normal"
gsettings set "$gnome_schema" icon-theme "Luv"
gsettings set "$gnome_schema" cursor-theme "oreo_frappe_pink_cursors"
gsettings set "$gnome_schema" font-name "Hack 10"
